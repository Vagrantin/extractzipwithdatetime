import zipfile
import os
import time
from datetime import datetime

zip_file = "C:\\temp\\Workflows.zip"
destination = "C:\\temp\\"
start_date = datetime(2023, 3, 8, 11, 0, 0)
end_date = datetime(2023, 3, 8, 11, 59, 0)

with zipfile.ZipFile(zip_file) as zip_file:
    print(f"file: {zip_file}\n")
    print(f"infolist: {zip_file.infolist}\n")
    for file_info in zip_file.infolist():
        print(f"file_info: {file_info}\n")
        print(f"datetime: {datetime(*file_info.date_time)}\n")
        print(f"start_date: {start_date}\n")
        print(f"end_date: {end_date}\n")
        date_time = datetime(*file_info.date_time)
        if start_date <= date_time <= end_date:
            zip_file.extract(file_info.filename, destination)
            print(f"filename: {zip_file.filename}\n")

for root, dirs, files in os.walk(destination):
    for file in files:
        file_path = os.path.join(root, file)
        modified_time = datetime.fromtimestamp(os.path.getmtime(file_path))
        if start_date <= modified_time <= end_date:
            print(f"Extracting file {file_path}")

