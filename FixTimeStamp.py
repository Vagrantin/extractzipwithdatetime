import zipfile
import os
import time
from datetime import datetime

zip_file = "C:\\temp\\Workflows.zip"
destination = "C:\\temp\\"

# Restores the timestamps of zipfile contents.
def RestoreTimestampsOfZipContents(zip_file, destination):
    for f in zipfile.ZipFile(zip_file, 'r').infolist():
        # path to this extracted f-item
        fullpath = os.path.join(destination, f.filename)
        # still need to adjust the dt o/w item will have the current dt
        date_time = time.mktime(f.date_time + (0, 0, -1))
        # update dt
        try:
            os.utime(fullpath, (date_time, date_time))
        except FileNotFoundError:
            print(f"Fine with me")

RestoreTimestampsOfZipContents(zip_file,destination)
